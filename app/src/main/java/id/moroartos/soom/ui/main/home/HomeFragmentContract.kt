package id.moroartos.soom.ui.main.home

import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.JsonIOException
import id.moroartos.soom.data.datasource.ApiClient
import id.moroartos.soom.data.model.MeetingModel
import id.moroartos.soom.data.model.StartMeetingModel
import id.moroartos.soom.utils.logD
import id.moroartos.soom.utils.logE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONException
import retrofit2.HttpException
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

class HomeFragmentPresenter(private val db:FirebaseFirestore,private val view: HomeFragmentView) {
    private val disposable = CompositeDisposable()
    fun getMyMeetings(email:String){
        view.isLoading(true)
        db.collection("meetings")
            .whereEqualTo("email",email)
            .get().addOnCompleteListener {
                if(it.isSuccessful){
                    if(it.result!=null){
                        val data= mutableListOf<MeetingModel?>()
                        it.result!!.documents.forEach { doc->
                            data.add(
                                MeetingModel(doc.id,
                                    doc["email"] as String,
                                    doc["hostname"] as String,
                                    (doc["intervals"] as Long).toInt(),
                                    doc["passcode"] as String,
                                    doc["room_id"] as String,
                                    doc["title"] as String,
                                    doc["description"] as String,
                                    (doc["date_start"] as Timestamp).toDate(),
                                    (doc["date_end"] as Timestamp).toDate(),
                                    doc["status_done"]as String))
                        }
                        view.isLoading(false)
                        view.showData(data)
                    }else{
                        view.isError("You Haven't Meeting Yet!")
                        view.isLoading(false)
                    }
                }
            }.addOnFailureListener {

            }
    }
    fun triggerStopBot(roomId:String,passcode:String,meetingId:String){


    }

    fun triggerStartBot(roomId:String,passcode:String,meetingId:String,interval:Int){
        try {
            disposable.add(
                ApiClient.createTriggerService()
                    .startMeeting(roomId, passcode, meetingId, interval)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        logD(it)
                    }, {
                        if(it is HttpException){
                            logE("http ex")
                        }
                        if(it is JsonIOException){
                            logE("json ex")
                        }
                        view.isError("Error Occurred")
                        logE(it.localizedMessage)
                        logE(it.printStackTrace().toString())
                    })
            )
        }catch (ex:Exception) {
            ex.printStackTrace()
            logE(ex.localizedMessage)
        }
    }

    fun triggerStopBot(meetingId: String){
        try {
            disposable.add(
                    ApiClient.createTriggerService()
                            .stopMeeting()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({
                                logD(it)
                                updateMeetingStatus(meetingId)
                            }, {
                                if(it is HttpException){
                                    logE("http ex")
                                    view.isError("Error Occurred")
                                }
                                if(it is JsonIOException){
                                    logE("json ex")
                                    updateMeetingStatus(meetingId)
                                }

                                logE(it.localizedMessage)
                                logE(it.printStackTrace().toString())
                            })
            )
        }catch (ex:Exception) {
            ex.printStackTrace()
            logE(ex.localizedMessage)
        }
    }

    private fun updateMeetingStatus(meetingId: String){
        val update = mapOf<String,String>(
                "status_done" to "done"
        )
        db.collection("meetings").document(meetingId).update(update)
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        view.isLoading(false)
                        view.isSuccess("Meeting Recap Processed")
                    }
                }
                .addOnFailureListener {
                    logE(it.localizedMessage)
                    view.isLoading(false)
                    view.isError("Error Occurred")
                }
    }


}
interface HomeFragmentView{
    fun isLoading(state:Boolean)
    fun isError(msg:String)
    fun isSuccess(msg: String)
    fun showData(data:List<MeetingModel?>)

}