package id.moroartos.soom.ui.detail

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.google.firebase.firestore.FirebaseFirestore
import id.moroartos.soom.base.BaseActivity
import id.moroartos.soom.data.model.MeetingModel
import id.moroartos.soom.data.model.ParticipantModel
import id.moroartos.soom.data.model.Participants
import id.moroartos.soom.data.model.RecapModel
import id.moroartos.soom.databinding.ActivityDetailBinding
import id.moroartos.soom.ui.attendance.DetailAttendanceActivity
import id.moroartos.soom.utils.logE
import id.moroartos.soom.utils.toast

class DetailActivity : BaseActivity<ActivityDetailBinding>() {
    override val bindingInflater: (LayoutInflater) -> ActivityDetailBinding
        get() = ActivityDetailBinding::inflate
    private lateinit var data:MeetingModel

    private val meetingRecaps= mutableListOf<RecapModel>()
    private lateinit var db:FirebaseFirestore
    private val meetingParticipants= mutableListOf<ParticipantModel>()

    private val classification0= mutableListOf<BarEntry>()
    private val classification1= mutableListOf<BarEntry>()
    private val classification2= mutableListOf<BarEntry>()
    private val classification3= mutableListOf<BarEntry>()
    private val classification4= mutableListOf<BarEntry>()
    private val classification5= mutableListOf<BarEntry>()
    private val classification6= mutableListOf<BarEntry>()
    private val classification7= mutableListOf<BarEntry>()


    override fun setupView(binding: ActivityDetailBinding) {
        data = intent.getParcelableExtra<MeetingModel>("data")!!
        db= FirebaseFirestore.getInstance()
        binding.ivBack.setOnClickListener {
            onBackPressed()
        }
        showData()
        getData()
        getParticipants()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun showData(){
        binding.tvTitle.text=data.title
        binding.tvTopic.text=data.title
        binding.tvDesc.text=data.description
        binding.tvCaptureInterval.text="${data.intervals}  Minutes"
    }

    private fun getParticipants(){
        meetingParticipants.clear()
        db.collection("meeting_participants").whereEqualTo("meeting_id",data.id)
                .get()
                .addOnFailureListener {
                    logE(it.localizedMessage.toString())
                    toast("Error : ${it.localizedMessage}")
                }
                .addOnCompleteListener {
                    if(it.result!=null){
                        it.result?.documents?.forEach { doc->
                            meetingParticipants.add(doc.toObject(ParticipantModel::class.java)!!)
                        }

                        binding.tvAttendance.text="${meetingParticipants.size} Person"
                        binding.ivArrow.setOnClickListener {
                            val intent= Intent(this@DetailActivity,DetailAttendanceActivity::class.java)
                            val participants = Participants(meetingParticipants)
                            intent.putExtra("attendances",participants)
                            startActivity(intent)
                        }
                    }
                }

    }
    


    private fun getData(){
        meetingRecaps.clear()
        db.collection("meeting_recaps").whereEqualTo("meeting_id",data.id)
                .get()
                .addOnCompleteListener {
                    if(it.result!=null){
                        it.result?.documents?.forEach { doc->
                            val document=doc.toObject(RecapModel::class.java)
                            meetingRecaps.add(document!!)
                        }
                        renderData()
                    }else{
                        logE("no recaps data")
                    }
                }
                .addOnFailureListener {
                    logE(it.localizedMessage)
                    toast("Error ${it.localizedMessage}")
                }
    }


    private fun renderData(){
        resetEntry()
        meetingRecaps.sortBy { it.timestamp }
        val intervals= mutableListOf<String>()
        meetingRecaps.forEachIndexed{i, it ->
            classification0.add(BarEntry((i+1f),it.classification_0.toFloat()))
            classification1.add(BarEntry((i+1f),it.classification_1.toFloat()))
            classification2.add(BarEntry((i+1f),it.classification_2.toFloat()))
            classification3.add(BarEntry((i+1f),it.classification_3.toFloat()))
            classification4.add(BarEntry((i+1f),it.classification_4.toFloat()))
            classification5.add(BarEntry((i+1f),it.classification_5.toFloat()))
            classification6.add(BarEntry((i+1f),it.classification_6.toFloat()))
            classification7.add(BarEntry((i+1f),it.classification_7.toFloat()))
            intervals.add("Intervals $i")
        }

        val dataSetClassification0=BarDataSet(classification0,"Inactive")
        val dataSetClassification1=BarDataSet(classification1,"Angry")
        val dataSetClassification2=BarDataSet(classification2,"Sad")
        val dataSetClassification3=BarDataSet(classification3,"Fear")
        val dataSetClassification4=BarDataSet(classification4,"Disgusted")
        val dataSetClassification5=BarDataSet(classification5,"Neutral")
        val dataSetClassification6=BarDataSet(classification6,"Surprise")
        val dataSetClassification7=BarDataSet(classification7,"Happy")

        dataSetClassification0.color = Color.BLACK
        dataSetClassification1.color = Color.RED
        dataSetClassification2.color = Color.YELLOW
        dataSetClassification3.color = Color.GRAY
        dataSetClassification4.color = Color.CYAN
        dataSetClassification5.color = Color.BLUE
        dataSetClassification6.color = Color.MAGENTA
        dataSetClassification7.color = Color.GREEN

        val dataSets=BarData(dataSetClassification0,dataSetClassification1,dataSetClassification2,dataSetClassification3
        ,dataSetClassification4, dataSetClassification5,dataSetClassification6,dataSetClassification7)

        binding.barChart.data=dataSets
        val xAxis=binding.barChart.xAxis
        xAxis.valueFormatter=IndexAxisValueFormatter(intervals)
        xAxis.setCenterAxisLabels(true)
        xAxis.position=XAxis.XAxisPosition.BOTTOM
        xAxis.granularity=1.1f
        xAxis.isGranularityEnabled=true

        binding.barChart.isDragEnabled=true

        val barSpace=0.01f
        val groupSpace=0.2f
        dataSets.barWidth=0.10f

        binding.barChart.xAxis.axisMinimum=0f
        binding.barChart.xAxis.mAxisMaximum=(0+binding.barChart.barData.getGroupWidth(groupSpace,barSpace)*meetingRecaps.size)

        binding.barChart.axisLeft.axisMinimum=0f
        binding.barChart.groupBars(0f,groupSpace,barSpace)

        binding.barChart.invalidate()

    }

    private fun resetEntry(){
        classification0.clear()
        classification1.clear()
        classification2.clear()
        classification3.clear()
        classification4.clear()
        classification5.clear()
        classification6.clear()
        classification7.clear()
    }

}