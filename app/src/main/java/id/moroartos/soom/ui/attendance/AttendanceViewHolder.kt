package id.moroartos.soom.ui.attendance

import androidx.recyclerview.widget.RecyclerView
import id.moroartos.soom.R
import id.moroartos.soom.base.RvAdapter
import id.moroartos.soom.data.model.MeetingModel
import id.moroartos.soom.data.model.ParticipantModel
import id.moroartos.soom.databinding.ItemResultBinding
import id.moroartos.soom.databinding.ItemUserBinding
import id.moroartos.soom.utils.loadProfile

class AttendanceViewHolder(private val binding: ItemUserBinding) : RecyclerView.ViewHolder(binding.root),
    RvAdapter.Binder<ParticipantModel> {
    override fun bindData(data: ParticipantModel, listen: (ParticipantModel) -> Unit, position: Int) {
        binding.tvUsername.text=data.name.replace("%","").trim()
        binding.ivAvaUser.loadProfile(data.name)
        if(data.status == 0L){
            binding.tvStatus.text="Absent"
            binding.tvStatus.background=itemView.resources.getDrawable(R.drawable.emotion_indikator_red)
        }else{
            binding.tvStatus.text="Attend"
            binding.tvStatus.background=itemView.resources.getDrawable(R.drawable.emotion_indikator)
        }
        itemView.setOnClickListener{listen(data)
        }
    }
}