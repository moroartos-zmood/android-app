package id.moroartos.soom.ui.attendance

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import id.moroartos.soom.R
import id.moroartos.soom.base.BaseActivity
import id.moroartos.soom.base.RvAdapter
import id.moroartos.soom.data.model.MeetingModel
import id.moroartos.soom.data.model.ParticipantModel
import id.moroartos.soom.data.model.Participants
import id.moroartos.soom.databinding.ActivityDetailAttendanceBinding
import id.moroartos.soom.databinding.ItemUserBinding

class DetailAttendanceActivity : BaseActivity<ActivityDetailAttendanceBinding>() {
    override val bindingInflater: (LayoutInflater) -> ActivityDetailAttendanceBinding
        get() = ActivityDetailAttendanceBinding::inflate
    private val participants = mutableListOf<ParticipantModel>()
    private val adapter = object :RvAdapter<ParticipantModel,ItemUserBinding>(participants,{

    }){
        override fun getViewBinding(parent: ViewGroup, viewType: Int): ItemUserBinding =
            ItemUserBinding.inflate(layoutInflater,parent,false)

        override fun viewHolder(binding: ItemUserBinding, viewType: Int): RecyclerView.ViewHolder =
            AttendanceViewHolder(binding)

        override fun layoutId(position: Int, obj: ParticipantModel): Int = R.layout.item_user

    }


    override fun setupView(binding: ActivityDetailAttendanceBinding) {
        binding.rvAttendance.apply {
            layoutManager=LinearLayoutManager(this@DetailAttendanceActivity)
            adapter=this@DetailAttendanceActivity.adapter
        }
        if(intent.hasExtra("attendances")){
            participants.clear()
            participants.addAll(intent.getParcelableExtra<Participants>("attendances")!!.data)
            adapter.refreshData(participants)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

}