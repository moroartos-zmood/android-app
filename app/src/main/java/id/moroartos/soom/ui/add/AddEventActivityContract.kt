package id.moroartos.soom.ui.add

import com.google.firebase.firestore.FirebaseFirestore
import id.moroartos.soom.data.datasource.ApiClient
import id.moroartos.soom.data.model.CreateMeetingResponse
import id.moroartos.soom.data.model.StartMeetingModel
import id.moroartos.soom.utils.logD
import id.moroartos.soom.utils.logE
import id.moroartos.soom.utils.toFormat
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class AddEventActivityPresenter(private val db:FirebaseFirestore, private val view: AddEventActivityView){
    private val disposable= CompositeDisposable()
    fun scheduleMeeting(data:StartMeetingModel){
        view.isLoading(true)
        disposable.add(
            ApiClient.createService().startMeeting(data.title,data.date_start.toFormat())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    insertSchedule(data,it)
                    logD(it.join_url)
                },{
                    view.isLoading(false)
                    logE(it.localizedMessage)
                    view.isError(it.localizedMessage)
                })
        )
    }

    private fun insertSchedule(data: StartMeetingModel, response:CreateMeetingResponse){
        data.room_id=response.id
        data.passcode=response.password
        data.status_done="no done"
        logD("Ready to insert")
        db.collection("meetings").add(data).addOnCompleteListener {
            if(it.isSuccessful){
                view.isSuccess("Your Scheduled Meeting Already Created")
                view.isLoading(false)
            }else if(it.exception!=null){
                view.isError("Error Occured")
                logE(it.exception!!.localizedMessage)
            }
            view.isLoading(false)
        }
    }
}
interface AddEventActivityView{
    fun isLoading(state:Boolean)
    fun isError(msg:String)
    fun isSuccess(msg: String)
}