package id.moroartos.soom.ui.main.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.moroartos.soom.R

class EditProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
    }
}