package id.moroartos.soom.ui.main.profile

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import id.moroartos.soom.base.BaseFragment
import id.moroartos.soom.data.local.SharedPreference
import id.moroartos.soom.databinding.FragmentProfileBinding
import id.moroartos.soom.ui.login.LoginActivity
import id.moroartos.soom.ui.main.MainActivity
import id.moroartos.soom.utils.loadProfile
import us.zoom.sdk.ZoomSDK

class ProfileFragment:BaseFragment<FragmentProfileBinding>() {

    companion object{
        fun getInstance()=ProfileFragment()
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentProfileBinding
        get() = FragmentProfileBinding::inflate

    override fun setupView(binding: FragmentProfileBinding) {
        val shared=SharedPreference(requireContext())
        binding.ivAva.loadProfile(shared.email!!)
        binding.tvEmail.text=shared.email!!
        binding.tvUname.text=shared.email!!
        binding.btnLogout.setOnClickListener {
            ZoomSDK.getInstance().logoutZoom()
            SharedPreference(requireContext()).isLogged=false
            (activity as MainActivity).startActivity(Intent(requireContext(),LoginActivity::class.java))
            (activity as MainActivity).finish()
        }
    }
}