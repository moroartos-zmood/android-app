package id.moroartos.soom.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import id.moroartos.soom.base.BaseActivity
import id.moroartos.soom.data.local.SharedPreference
import id.moroartos.soom.databinding.ActivityLoginBinding
import id.moroartos.soom.ui.main.MainActivity
import id.moroartos.soom.ui.main.MainActivityView
import id.moroartos.soom.utils.logE
import id.moroartos.soom.utils.toGone
import id.moroartos.soom.utils.toVisible
import id.moroartos.soom.utils.toast
import us.zoom.sdk.*

class LoginActivity : BaseActivity<ActivityLoginBinding>() {


    override val bindingInflater: (LayoutInflater) -> ActivityLoginBinding
        get() = ActivityLoginBinding::inflate



    override fun setupView(binding: ActivityLoginBinding) {
        initSdk()
        if(SharedPreference(this).isLogged){
            startActivity(Intent(this,MainActivity::class.java))
            finish()
        }
        binding.btnLogin.setOnClickListener {
            if(binding.etEmail.text.isNullOrEmpty()||binding.etPassword.text.isNullOrEmpty()){
                toast("Semua Data Harus Terisi!")
            }else {
                binding.pgBar.toVisible()
                login(binding.etEmail.text.toString(),binding.etPassword.text.toString())
            }
        }
    }
    private fun initSdk(){
        val sdk = ZoomSDK.getInstance()
        val params = ZoomSDKInitParams()
        params.appKey = "691BQfWxQ2PSevHyoyBlH50Xegn1FaUmOg84"

        params.appSecret = "GDIUIPDmdaqAy41WSaA3OZeaX6HIsxDSjmiM"

        params.domain = "zoom.us"
        params.enableLog = true
        val listener: ZoomSDKInitializeListener = object : ZoomSDKInitializeListener {
            override fun onZoomSDKInitializeResult(errorCode: Int, internalErrorCode: Int) {
                logE(errorCode.toString())

            }
            override fun onZoomAuthIdentityExpired() {}
        }
        sdk.initialize(this, listener, params)
    }
    private fun login(username: String, password: String) {
        val result = ZoomSDK.getInstance().loginWithZoom(username, password)
        if (result == ZoomApiError.ZOOM_API_ERROR_SUCCESS) {
            ZoomSDK.getInstance().addAuthenticationListener(authListener)
        }
    }
    private val authListener: ZoomSDKAuthenticationListener = object :
        ZoomSDKAuthenticationListener {
        override fun onZoomSDKLoginResult(result: Long) {
            binding.pgBar.toGone()
            logE(result.toString())
            if (result == ZoomAuthenticationError.ZOOM_AUTH_ERROR_SUCCESS.toLong()) {
                val pref=SharedPreference(this@LoginActivity)
                pref.email=binding.etEmail.text.toString()
                pref.isLogged=true
                startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                finish()
            }else{
                toast("Login Gagal!")
            }
        }

        override fun onZoomSDKLogoutResult(l: Long) {}
        override fun onZoomIdentityExpired() {}
        override fun onZoomAuthIdentityExpired() {}
    }
}