package id.moroartos.soom.ui.add

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import id.moroartos.soom.R
import id.moroartos.soom.base.BaseActivity
import id.moroartos.soom.data.local.SharedPreference
import id.moroartos.soom.data.model.StartMeetingModel
import id.moroartos.soom.databinding.ActivityAddEventBinding
import id.moroartos.soom.utils.*
import java.text.SimpleDateFormat
import java.util.*

class AddEventActivity : BaseActivity<ActivityAddEventBinding>(), AddEventActivityView {



    override val bindingInflater: (LayoutInflater) -> ActivityAddEventBinding
        get() = ActivityAddEventBinding::inflate
    private val presenter= AddEventActivityPresenter(FirebaseFirestore.getInstance(),this)

    override fun setupView(binding: ActivityAddEventBinding) {
        val nama=SharedPreference(this).email!!
        binding.btnAddSchedule.setOnClickListener {
            if (binding.etEventSchedule.text.toString() == ""||binding.etEventName.text.toString()=="") {
                toast("Data Harus Lengkap")
            } else{
            val data = StartMeetingModel(SharedPreference(this).email!!,
                    nama, binding.etEventInterval.text.toString().toInt(),
                    "", "", binding.etEventName.text.toString(), binding.etEventDesc.text.toString(), binding.etEventSchedule.text.toString().toDate(), binding.etEventSchedule.text.toString().toDate(), "no_done")
            presenter.scheduleMeeting(data)
        }
        }
        binding.etEventSchedule.setOnClickListener {
            pickDateTime()
        }
    }

    override fun isLoading(state: Boolean) {
        logE(state.toString())
        if(state){
            binding.pgLoading.toVisible()
        }else{
            binding.pgLoading.toGone()
        }
    }

    private fun pickDateTime() {
        val currentDateTime = Calendar.getInstance()
        val startYear = currentDateTime.get(Calendar.YEAR)
        val startMonth = currentDateTime.get(Calendar.MONTH)
        val startDay = currentDateTime.get(Calendar.DAY_OF_MONTH)
        val startHour = currentDateTime.get(Calendar.HOUR_OF_DAY)
        val startMinute = currentDateTime.get(Calendar.MINUTE)

        DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { _, year, month, day ->
                    TimePickerDialog(
                            this,
                            TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                                val pickedDateTime = Calendar.getInstance()
                                pickedDateTime.set(year, month, day, hour, minute)
                                        binding.etEventSchedule.setText(pickedDateTime.time.toFormat())
                            },
                            startHour,
                            startMinute,
                            false
                    ).show()
                },
                startYear,
                startMonth,
                startDay
        ).show()
    }

    override fun isError(msg: String) {
        logE(msg)
    }

    override fun isSuccess(msg: String) {
        logE(msg)
        toast(msg)
        finish()
    }
}