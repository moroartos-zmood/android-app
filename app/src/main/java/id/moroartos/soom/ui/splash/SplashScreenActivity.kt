package id.moroartos.soom.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import id.moroartos.soom.base.BaseActivity
import id.moroartos.soom.databinding.ActivitySplashScreenBinding
import id.moroartos.soom.ui.login.LoginActivity

class SplashScreenActivity : BaseActivity<ActivitySplashScreenBinding>() {
    override val bindingInflater: (LayoutInflater) -> ActivitySplashScreenBinding
        get() = ActivitySplashScreenBinding::inflate

    private var runnable:Handler?=null
    override fun setupView(binding: ActivitySplashScreenBinding) {
        runnable= Handler()
        runnable?.postDelayed({
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        },1500)
    }

    override fun onDestroy() {
        super.onDestroy()
        runnable=null
    }


}