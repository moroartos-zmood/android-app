package id.moroartos.soom.ui.main.home

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore
import id.moroartos.soom.R
import id.moroartos.soom.base.BaseFragment
import id.moroartos.soom.base.RvAdapter
import id.moroartos.soom.data.local.SharedPreference
import id.moroartos.soom.data.model.MeetingModel
import id.moroartos.soom.databinding.FragmentHomeBinding
import id.moroartos.soom.databinding.ItemResultBinding
import id.moroartos.soom.ui.detail.DetailActivity
import id.moroartos.soom.ui.main.MainActivity
import id.moroartos.soom.utils.*
import us.zoom.sdk.*
import java.lang.Exception


class HomeFragment: BaseFragment<FragmentHomeBinding>(), HomeFragmentView {

    companion object{
        fun getInstance():HomeFragment = HomeFragment()
    }
    private val meetings= mutableListOf<MeetingModel?>()

    private var started=false
    private var ended=false

    private val adapter=object :RvAdapter<MeetingModel?, ItemResultBinding>(meetings, {
//       joinMeet(it)
       performClick(it)
    }){
        override fun getViewBinding(parent: ViewGroup, viewType: Int): ItemResultBinding =
                ItemResultBinding.inflate(layoutInflater, parent, false)

        override fun layoutId(position: Int, obj: MeetingModel?): Int = R.layout.item_result

        override fun viewHolder(
                binding: ItemResultBinding,
                viewType: Int
        ): RecyclerView.ViewHolder  = ItemMeetingViewHolder(binding)
    }

    private lateinit var presenter: HomeFragmentPresenter

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentHomeBinding
        get() = FragmentHomeBinding::inflate


    private fun performClick(it:MeetingModel?){
        logE(it!!.is_done)
        if(it.is_done=="done"){
            val intent = Intent(requireContext(),DetailActivity::class.java)
            intent.putExtra("data",it)
            requireContext().startActivity(intent)
        }else{
            (activity as MainActivity).showDialog(it)
        }
    }


    override fun setupView(binding: FragmentHomeBinding) {
        presenter=HomeFragmentPresenter(FirebaseFirestore.getInstance(), this)
        val shared=SharedPreference(requireContext())
        binding.ivProfile.loadProfile(shared.email!!)
        binding.tvName.text=shared.email
        binding.ivSearch.setOnClickListener {
            binding.svEvent.toVisible()
            binding.svEvent.isFocusable=true
            binding.svEvent.requestFocus()
            binding.ivProfile.toInvisible()
            binding.tvName.toInvisible()
            binding.tvSelamat.toInvisible()
            binding.ivSearch.toInvisible()
        }
        binding.svEvent.setOnCloseListener {
            binding.svEvent.toGone()
            binding.ivProfile.toVisible()
            binding.tvName.toVisible()
            binding.tvSelamat.toVisible()
            binding.ivSearch.toVisible()
            return@setOnCloseListener true
        }
        binding.rvEvents.apply {
            val layManager=LinearLayoutManager(this@HomeFragment.requireContext())
            layManager.orientation=LinearLayoutManager.VERTICAL
            layoutManager=layManager
            adapter=this@HomeFragment.adapter
        }
        adapter.notifyDataSetChanged()

    }
    fun joinMeet(it: MeetingModel?, checked: Boolean){
        try {
            val meetingService = ZoomSDK.getInstance().meetingService
            val options = JoinMeetingOptions()
            val params = JoinMeetingParams()
            params.displayName = SharedPreference(requireContext()).email!!
            params.meetingNo = it?.room_id
            params.password = it?.passcode
            meetingService.addListener(object : MeetingServiceListener {
                override fun onMeetingStatusChanged(p0: MeetingStatus?, p1: Int, p2: Int) {
                    logE("listener ${p0?.name}")
                    if (p0?.name == "MEETING_STATUS_INMEETING") {
                        if(!started) {
                            started = true
                            presenter.triggerStartBot(it!!.room_id, it.passcode, it.id, it.intervals)
                            (activity as MainActivity).hideDialog()
                            logE("Triggerr")
                        }
                    }
                    if (p0?.name == "MEETING_STATUS_DISCONNECTING") {
                        if(!ended) {
                            started = false
                            ended=true
                            requireContext().toast("Meeting Stopped, Processing Your Recap!")
                            presenter.triggerStopBot(it!!.id)
                        }
                    }
                }
            })
            meetingService.joinMeetingWithParams(context, params, options)
        }catch (ex:Exception){
            ex.printStackTrace()
            logE(ex.localizedMessage)
        }
    }

    private fun schedulingMeeting(){
        val zoomSDK= ZoomSDK.getInstance()
        if(zoomSDK.isInitialized){
            val accountService=zoomSDK.accountService
            if(accountService==null){
                requireContext().toast("Account Service is Null")
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val shared=SharedPreference(requireContext())
        presenter.getMyMeetings(shared.email!!)
    }

    override fun isLoading(state: Boolean) {
        changeState(state)
    }

    private fun changeState(state: Boolean){
        when(state){
            true-> binding.pbLoading.toVisible()
            else-> binding.pbLoading.toGone()
        }
    }
    override fun isError(msg: String) {

    }

    override fun isSuccess(msg: String) {

    }

    override fun showData(data: List<MeetingModel?>) {
        logE("${data.size}")
        meetings.clear()
        meetings.addAll(data)
        adapter.notifyDataSetChanged()
        logE("${meetings.size}")
    }



}