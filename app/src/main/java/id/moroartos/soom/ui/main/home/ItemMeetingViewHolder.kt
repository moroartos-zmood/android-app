package id.moroartos.soom.ui.main.home

import androidx.recyclerview.widget.RecyclerView
import id.moroartos.soom.base.RvAdapter
import id.moroartos.soom.data.model.MeetingModel
import id.moroartos.soom.databinding.ItemResultBinding

class ItemMeetingViewHolder (private val binding: ItemResultBinding) : RecyclerView.ViewHolder(binding.root),
RvAdapter.Binder<MeetingModel?> {
    override fun bindData(data: MeetingModel?, listen: (MeetingModel?) -> Unit, position: Int) {
        binding.tvDate.text="${data?.date_start}"
        binding.tvEventId.text=data?.room_id
        binding.tvName.text=data?.title
        binding.tvSchedule.text="${data?.date_start}"
        itemView.setOnClickListener{listen(data)}
    }
}