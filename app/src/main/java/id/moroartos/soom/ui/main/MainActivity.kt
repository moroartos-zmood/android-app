package id.moroartos.soom.ui.main

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.firebase.firestore.FirebaseFirestore
import id.moroartos.soom.R
import id.moroartos.soom.base.BaseActivity
import id.moroartos.soom.data.datasource.ApiClient
import id.moroartos.soom.data.model.MeetingModel
import id.moroartos.soom.databinding.ActivityMainBinding
import id.moroartos.soom.databinding.BottomSheetJoinMeetingBinding
import id.moroartos.soom.ui.add.AddEventActivity
import id.moroartos.soom.ui.main.home.HomeFragment
import id.moroartos.soom.ui.main.profile.ProfileFragment
import id.moroartos.soom.utils.logE
import id.moroartos.soom.utils.toGone
import id.moroartos.soom.utils.toVisible
import us.zoom.sdk.*


class MainActivity :BaseActivity<ActivityMainBinding>(),MainActivityView {

    override val bindingInflater: (LayoutInflater) -> ActivityMainBinding
        get() = ActivityMainBinding::inflate

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>
    private lateinit var sheetLayout: LinearLayout
    private lateinit var sheetBinding:BottomSheetJoinMeetingBinding



    override fun setupView(binding: ActivityMainBinding) {
        getConfig()
        binding.bottomNav.setOnItemSelectedListener(object : BottomNavigationView.OnNavigationItemSelectedListener{
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                when(item.itemId){
                    R.id.home->{
                        changeFragment(HomeFragment.getInstance())
                        return true
                    }
                    R.id.profil->{
                        changeFragment(ProfileFragment.getInstance())
                        return true
                    }
                }
                return false
            }
        })
        sheetLayout= findViewById(R.id.bs_add_document)
        bottomSheetBehavior= BottomSheetBehavior.from(sheetLayout)
        sheetBinding= BottomSheetJoinMeetingBinding.bind(sheetLayout)
        bottomSheetBehavior.peekHeight=0
        bottomSheetBehavior.addBottomSheetCallback(object :BottomSheetBehavior.BottomSheetCallback(){
            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if(newState==BottomSheetBehavior.STATE_COLLAPSED){
                    binding.viewTransparent.toGone()
                    binding.fabButton.toVisible()
                }else{
                    binding.viewTransparent.toVisible()
                    binding.fabButton.toGone()
                }
            }
        })
        bottomSheetBehavior.state=BottomSheetBehavior.STATE_COLLAPSED
        binding.bottomNav.selectedItemId=R.id.home
        binding.fabButton.setOnClickListener {
            startActivity(Intent(this, AddEventActivity::class.java))
        }


    }
    private fun getConfig(){
        FirebaseFirestore.getInstance().collection("config").document("config_url")
                .get()
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        val result= it.result?.get("url") as String
                        logE("url $result" )
                        ApiClient.BASE_URL=result
                        logE(ApiClient.BASE_URL)
                    }
                }
                .addOnFailureListener {  }
    }

    fun showDialog(dataModel:MeetingModel?){
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        sheetBinding.btnStartMeeting.setOnClickListener {
            val fr=supportFragmentManager.findFragmentByTag(HomeFragment::class.java.simpleName) as HomeFragment
            fr.joinMeet(dataModel,sheetBinding.swVideo.isChecked)
        }
    }
    fun hideDialog(){
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }


    private fun changeFragment(f:Fragment,){
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(binding.frameContainer.id,f,f::class.java.simpleName)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun initSdk(){

        val sdk = ZoomSDK.getInstance()
        val params = ZoomSDKInitParams()
        params.appKey = "691BQfWxQ2PSevHyoyBlH50Xegn1FaUmOg84" // TODO: Retrieve your SDK key and enter it here

        params.appSecret = "GDIUIPDmdaqAy41WSaA3OZeaX6HIsxDSjmiM" // TODO: Retrieve your SDK secret and enter it here

        params.domain = "zoom.us"
        params.enableLog = true
        val listener: ZoomSDKInitializeListener = object : ZoomSDKInitializeListener {
            override fun onZoomSDKInitializeResult(errorCode: Int, internalErrorCode: Int) {
                logE(errorCode.toString())
                login("danang@divistant.com","Wkwkwk123")
            }
            override fun onZoomAuthIdentityExpired() {}
        }
        sdk.initialize(this, listener, params)


    }


    private fun login(username: String, password: String) {
        val result = ZoomSDK.getInstance().loginWithZoom(username, password)
        if (result == ZoomApiError.ZOOM_API_ERROR_SUCCESS) {
            ZoomSDK.getInstance().addAuthenticationListener(authListener)
        }
    }

    // 3. Write the startMeeting function
    private fun startMeeting(context: Context) {
        val sdk = ZoomSDK.getInstance()
        if (sdk.isLoggedIn) {
            val meetingService = sdk.meetingService
            val options =MeetingOptions()
            meetingService.startInstantMeeting(context,options)
        }
    }
    private val authListener: ZoomSDKAuthenticationListener = object : ZoomSDKAuthenticationListener {
        override fun onZoomSDKLoginResult(result: Long) {
            logE(result.toString())
            if (result == ZoomAuthenticationError.ZOOM_AUTH_ERROR_SUCCESS.toLong()) {
                startMeeting(this@MainActivity)
            }
        }

        override fun onZoomSDKLogoutResult(l: Long) {}
        override fun onZoomIdentityExpired() {}
        override fun onZoomAuthIdentityExpired() {}
    }

    override fun isLoading(state: Boolean) {

    }

    override fun isError(msg: String) {

    }

    override fun isSuccess(msg: String) {

    }

}