package id.moroartos.soom.ui.main

class MainActivityPresenter(private val view: MainActivityView){
   fun getSchedule(){

   }
}
interface MainActivityView{
    fun isLoading(state:Boolean)
    fun isError(msg:String)
    fun isSuccess(msg: String)
}