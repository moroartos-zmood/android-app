package id.moroartos.soom.utils

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import java.text.SimpleDateFormat
import java.util.*


inline fun <reified T>T.logE(msg: String) = msg.let {
    Log.e(T::class.java.simpleName, msg)
}
inline fun <reified T>T.logD(msg: String) = msg.let{
    Log.d(T::class.java.simpleName, msg)
}
fun Context.toast(msg: String){
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}
fun View.toGone(){
    this.visibility=View.GONE
}
fun View.toVisible(){
    this.visibility=View.VISIBLE
}
fun View.toInvisible(){
    this.visibility=View.INVISIBLE
}

fun String.toDate(): Date {
    return SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'").parse(this)
}
fun Date.toFormat():String{
    return SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'").format(this)
}
fun ImageView.loadProfile(name: String){
    var requestOptions = RequestOptions()
    requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(50))
    Glide.with(this)
        .load("https://ui-avatars.com/api/?name=$name&background=6365f1&color=fff")
        .apply(requestOptions)
        .into(this)
}