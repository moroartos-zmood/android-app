package id.moroartos.soom.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Participants(
        var data:List<ParticipantModel>
) :Parcelable