package id.moroartos.soom.data.datasource

import com.google.gson.GsonBuilder
import id.moroartos.soom.data.service.ApiService
import id.moroartos.soom.data.service.ApiServiceTrigger
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class ApiClient {
    companion object{
        var BASE_URL="http://5537bbbf72d7.ngrok.io/hehe/"
        var gson = GsonBuilder()
                .setLenient()
                .create()

        fun getClient() = Retrofit.Builder().baseUrl("https://flaskappzm-t2yoqcmfca-et.a.run.app/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

        fun createService() = getClient().create(ApiService::class.java)


        fun getClientTrigger() = Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        fun createTriggerService()= getClientTrigger().create(ApiServiceTrigger::class.java)
    }
}