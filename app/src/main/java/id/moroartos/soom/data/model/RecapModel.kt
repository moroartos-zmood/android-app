package id.moroartos.soom.data.model

import android.os.Parcelable
import com.google.firebase.firestore.DocumentId
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class RecapModel(
        @DocumentId
        var id:String,
        var classification_0: Long,
        var classification_1: Long,
        var classification_2: Long,
        var classification_3: Long,
        var classification_4: Long,
        var classification_5: Long,
        var classification_6: Long,
        var classification_7: Long,
        var meeting_id: String,
        var timestamp: Long

):Parcelable {
    constructor():this("",0,0,0,0,0,0,0,0,"",0)
}