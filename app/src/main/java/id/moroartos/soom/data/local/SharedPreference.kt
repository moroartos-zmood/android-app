package id.moroartos.soom.data.local

import android.content.Context
import android.content.SharedPreferences

class SharedPreference(private val context: Context) {
    private val mode = 0
    private val prefName = "${context.packageName}.shared"
    private val pref: SharedPreferences = context.getSharedPreferences(prefName, mode)
    var isLogged: Boolean
        get() = pref.getBoolean("isLogged", false)
        set(value) = pref.edit().putBoolean("isLogged", value).apply()
    var email:String?
        get()=pref.getString("email","danang@divistant.com")
        set(value) = pref.edit().putString("email",value).apply()

}