package id.moroartos.soom.data.model

import android.os.Parcelable
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.PropertyName
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class MeetingModel(
    @DocumentId
    var id:String,
    var email:String,
    var hostname:String,
    var intervals:Int,
    var passcode:String,
    var room_id:String,
    var title:String,
    var description:String,
    var date_start: Date,
    var date_end:Date,
    var is_done:String
):Parcelable
{
    constructor():this("","","",0,"","","","",Date(),Date(),"")
}