package id.moroartos.soom.data.model

import com.google.firebase.firestore.Exclude
import com.google.type.DateTime
import java.util.*

data class StartMeetingModel (
        var email:String,
        var hostname:String,
        var intervals:Int,
        var passcode:String,
        var room_id:String,
        var title:String,
        var description:String,
        var date_start: Date,
        var date_end: Date,
        var status_done:String
    )