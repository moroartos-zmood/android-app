package id.moroartos.soom.data.service

import id.moroartos.soom.data.model.CreateMeetingResponse
import id.moroartos.soom.data.model.StartMeetingModel
import io.reactivex.Observable
import retrofit2.http.*

interface ApiServiceTrigger {

    @POST("/triggerBot")
    @FormUrlEncoded
    fun startMeeting(@Field("room_id") room_id:String,@Field("passcode")passcode:String,
                     @Field("meeting_id")meeting_id:String,@Field("intervals")interval:Int)
    : Observable<String>
    @GET("/endBot")
    fun stopMeeting()
            : Observable<String>
}