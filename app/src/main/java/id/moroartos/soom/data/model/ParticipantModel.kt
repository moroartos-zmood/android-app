package id.moroartos.soom.data.model

import android.os.Parcelable
import com.google.firebase.firestore.DocumentId
import kotlinx.parcelize.Parcelize

@Parcelize
data class ParticipantModel(
        @DocumentId
        var id :String,
        var meeting_id :String,
        var name:String,
        var status:Long
):Parcelable {
        constructor():this("","","",0)
}