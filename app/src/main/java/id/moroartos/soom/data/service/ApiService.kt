package id.moroartos.soom.data.service

import id.moroartos.soom.data.model.CreateMeetingResponse
import id.moroartos.soom.data.model.StartMeetingModel
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ApiService {

    @POST("createMeeting")
    @FormUrlEncoded
    fun startMeeting(@Field("topic") topic:String,@Field("start_time")start_time:String)
    : Observable<CreateMeetingResponse>
}