package id.moroartos.soom.data.model

data class CreateMeetingResponse(
    val agenda: String,
    val created_at: String,
    val duration: String,
    val host_id: String,
    val id: String,
    val join_url: String,
    val password: String,
    val registration_url: String,
    val start_time: String,
    val start_url: String,
    val status: String,
    val timezone: String,
    val topic: String,
    val type: String,
    val uuid: String
)