# Android Zmood App

This is an documentation for Zmood Android Application 


## Installation
Clone this repository and import into **Android Studio**
```bash
git clone git@gitlab.com:moroartos-zmood/android-app.git
```

## Build variants
Use the Android Studio *Build Variants* button to choose between **production** and **staging** flavors combined with debug and release build types


## Dependencies
1. Retrofit
2. Firebase Firestore
3. Zoom SDK
4. Rx Java

## Maintainers
This project is mantained by:
* [Danang Wijaya](http://github.com/danangwijaya750)
* [Alfiyah Q.Z](https://gitlab.com/alfiyahqothrunnada.2018)


## Contributing

1. Fork it
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -m 'Add some feature')
4. Run the linter (ruby lint.rb').
5. Push your branch (git push origin my-new-feature)
6. Create a new Pull Request
